#ifndef CIRCULARARRAY_H
#define CIRCULARARRAY_H
#include "stdafx.h"
#include <iostream>


template<class T>
class CircularArray
{
public:
	class iterator {
	public:
		iterator(CircularArray<T> cs);
		~iterator();
		virtual bool hasNext();
		virtual T next();
	private:
		int curr;
		CircularArray<T> sa;
	};
	CircularArray();
	CircularArray(unsigned int);
	~CircularArray();
	virtual CircularArray<T>::iterator getIterator();
	virtual void add(T);
	virtual unsigned int getSize();
	virtual std::ostream& printOn(std::ostream&);
	virtual T get(unsigned int);
	virtual T removeFront();
	virtual T peekFront();
	virtual T peekLast();
	virtual bool isEmpty();
	virtual void clear();

private:
	virtual T* allocate(unsigned int);
	unsigned int _pos;
	unsigned int _size;
	T* _data;	
};

template<class T>
CircularArray<T>::CircularArray() :
	_data(allocate(0)), _size(0), _pos(0)
{
}

template<class T>
CircularArray<T>::CircularArray(unsigned int size) :
	_data(allocate(size)), _size(size),_pos(0)
{
}

template<class T>
CircularArray<T>::~CircularArray()
{
}

template<class T>
CircularArray<T>::iterator CircularArray<T>::getIterator()
{
	return CircularArray<T>::iterator(*this);
}

template<class T>
void CircularArray<T>::add(T t)
{
	if (_pos > _size-1) {

		for (int i = 0; i < _size - 1; i++) {
			_data[i] = _data[i + 1];
		}
		_pos = _pos-1;
	}

	_data[_pos] = t;
	_pos += 1;
}

template<class T>
unsigned int CircularArray<T>::getSize()
{
	return _pos;
}

template<class T>
std::ostream& CircularArray<T>::printOn(std::ostream & s)
{
	s << "[";
	for (int i = 0; i < _pos; i++) {
		s << _data[i];
	}
	s << "]";
	return s;
}
template<class T>
T CircularArray<T>::get(unsigned int i)
{
	return _data[i];
}

template<class T>
T CircularArray<T>::removeFront()
{
	T front = _data[0];
	_data[0] = NULL;
	for (int i = 0; i < _pos - 1; i++) {
		_data[i] = _data[i + 1];
	}
	_data[_pos] = NULL;
	_pos -= 1;
	return front;
}

template<class T>
T CircularArray<T>::peekFront()
{
	return _data[0];
}

template<class T>
T CircularArray<T>::peekLast()
{
	return _data[_pos-1];
}

template<class T>
T* CircularArray<T>::allocate(unsigned int size)
{
	T* data = new T[size];
	for (int i = 0; i < size; i++) {
		data[i] = NULL;
	}
	return data;
}



template<class T>
bool CircularArray<T>::isEmpty()
{
	return _pos == 0;
}

template<class T>
void CircularArray<T>::clear()
{
	for (int i = 0; i < _pos; i++) {
		removeFront();
	}
	_pos = 0;
}


template<class T>
CircularArray<T>::iterator::iterator(CircularArray<T>& cs):
	sa(cs),curr(0)
{
}

template<class T>
CircularArray<T>::iterator::~iterator()
{
	delete[] sa;
}

template<class T>
bool CircularArray<T>::iterator::hasNext()
{
	return curr = sa.getSize();
}

template<class T>
T CircularArray<T>::iterator::next()
{
	T res = sa.get(curr);
	curr += 1;
	return res;
}


#endif
